// Day 30, First Missing Positive
#include <iostream>
#include <unordered_set>
#include <vector>
using std::vector;

class Solution {
    public:
        int firstMissingPositive(vector<int>& nums) {
            if (nums.empty()) return 1;
            const int N = nums.size();
            std::unordered_set<int> set(nums.begin(), nums.end());
            // Vector does not contain 1
            int min = 1;
            while (set.count(min)) ++min;
            return min;
        }
};

int main() {
    // Compile successful
    return 0;
}
