// Day 23, Gas Station
#include <cassert>
#include <iostream>
#include <vector>
using std::vector;

class Solution {
    public:
        int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
            const int n = gas.size();
            int tank = 0, index;
            for (int i = 0; i < n; ++i) {
                for (int j = 0; j < n; ++j) {
                    index = (i+j) % n;
                    tank += gas[index] - cost[index];
                    if (tank < 0)  {
                        tank = 0;
                        break;
                    }
                    if (j == n-1) return i;
                }
            }
            return -1;
        }
};

int main() {
    // Variables
    Solution sol;
    vector<int> gas, cost;
    int station;
    
    // Test cases
    // Test case 1
    gas  = {1};
    cost = {0};
    // Asserting it's non-empty
    assert(gas.size() == cost.size());
    assert(gas.size() > 0);
    // Test info
    std::cout << "Gas at each station: ";
    for (int x : gas) std::cout << x << " ";
    std::cout << std::endl;
    std::cout << "Cost to travel to the next station: ";
    for (int x : cost) std::cout << x << " ";
    std::cout << std::endl;
    station = sol.canCompleteCircuit(gas, cost);
    std::cout << "Can be completed at: " << station << std::endl << std::endl;
    
    // Test case 2
    gas  = {0};
    cost = {1};
    // Asserting it's non-empty
    assert(gas.size() == cost.size());
    assert(gas.size() > 0);
    // Test info
    std::cout << "Gas at each station: ";
    for (int x : gas) std::cout << x << " ";
    std::cout << std::endl;
    std::cout << "Cost to travel to the next station: ";
    for (int x : cost) std::cout << x << " ";
    std::cout << std::endl;
    station = sol.canCompleteCircuit(gas, cost);
    std::cout << "Can be completed at: " << station << std::endl << std::endl;
    
    // Test case 3
    gas  = {1,2,3,4,5};
    cost = {3,4,5,1,2};
    // Asserting it's non-empty
    assert(gas.size() == cost.size());
    assert(gas.size() > 0);
    // Test info
    std::cout << "Gas at each station: ";
    for (int x : gas) std::cout << x << " ";
    std::cout << std::endl;
    std::cout << "Cost to travel to the next station: ";
    for (int x : cost) std::cout << x << " ";
    std::cout << std::endl;
    station = sol.canCompleteCircuit(gas, cost);
    std::cout << "Can be completed at: " << station << std::endl << std::endl;
    
    // Test case 4
    gas  = {2,3,4};
    cost = {3,4,3};
    // Asserting it's non-empty
    assert(gas.size() == cost.size());
    assert(gas.size() > 0);
    // Test info
    std::cout << "Gas at each station: ";
    for (int x : gas) std::cout << x << " ";
    std::cout << std::endl;
    std::cout << "Cost to travel to the next station: ";
    for (int x : cost) std::cout << x << " ";
    std::cout << std::endl;
    station = sol.canCompleteCircuit(gas, cost);
    std::cout << "Can be completed at: " << station << std::endl << std::endl;
    
    /*
        Compile successful
    */
    return 0;
}
