// Day 28, Subarray Less Than K
#include <iostream>
#include <vector>
using std::vector;

class Solution {
    public:
        int numSubarrayProductLessThanK(vector<int>& nums, int k) {
            if (k <= 1) return 0;
            int product = 1, answer = 0;
            vector<int>::iterator lt = nums.begin();
            for (vector<int>::iterator rt = nums.begin(); rt != nums.end(); ++rt) {
                product *= *rt;
                while (product >= k) product /= *lt++;
                answer  += rt - lt + 1;
            }
            return answer;
        }
};

int main() {
    // Compile successful
    return 0;
}
