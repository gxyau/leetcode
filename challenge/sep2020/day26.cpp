// Day 26, Teemo Attacking
#include <iostream>
#include <vector>
using std::vector;

class Solution {
    public:
        int findPoisonedDuration(vector<int>& timeSeries, int duration) {
            if (timeSeries.empty()) return 0;
            int damage = duration, n = timeSeries.size();
            for (int i = 0; i < n-1; ++i) {
                damage += std::min(duration, timeSeries[i+1] - timeSeries[i]);
            }
            return damage;
        }
};

int main() {
    // Compile successful
    return 0;
}
