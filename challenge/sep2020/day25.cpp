// Largest Number
#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>
using std::string;
using std::vector;

class Solution {
    private:
        // Sum of element in vector
        bool zero(vector<int> &nums) {
            vector<int>::iterator it = nums.begin();
            while (it != nums.end()) {
                if (*it != 0) return false;
                ++it;
            }
            return true;
        }
        // Ordering of the numbers, larger combination comes first
        static bool partial_order(int num1, int num2) {
            string str1 = std::to_string(num1), str2 = std::to_string(num2);
            return (str1 + str2) > (str2 + str1);
        }
    public:
        string largestNumber(vector<int>& nums) {
            // Edge cases
            if (nums.empty()) return "";
            if (zero(nums)) return "0";
            // Initialising variables
            string ans;
            // Sorting numbers
            std::sort(nums.begin(), nums.end(), partial_order);
            for (auto it = nums.begin(); it != nums.end(); ++it) {
                ans += std::to_string(*it);
            }
            return ans;
        }
};

int main() {
    // Compile successful
    return 0;
}
