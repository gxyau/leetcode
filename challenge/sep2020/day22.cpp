// Day 22, Majority Elements II
#include <algorithm>
#include <cassert>
#include <iostream>
#include <vector>
using std::vector;

class Solution {
    public:
        vector<int> majorityElement(vector<int>& nums) {
            int n = nums.size();
            // Edge cases
            if (n <= 1) {
                return nums;
            } else if (n == 2) {
                return nums[0] == nums[1] ? vector<int>(1, {nums[0]}) : nums;
            }
            // Vectors of length at least 3
            // There are at most 2 majority elements since they have to be at least 1/3
            vector<int> majority;
            int elm1, elm2, count1 = 0, count2 = 0;
            for (auto it = nums.begin(); it != nums.end(); ++it) {
                if (*it == elm1) {
                    ++count1;
                } else if (*it == elm2) {
                    ++count2;
                } else if (count1 == 0) {
                    elm1 = *it;
                    ++count1;
                } else if (count2 == 0) {
                    elm2 = *it;
                    ++count2;
                } else {
                    --count1;
                    --count2;
                }
            }
            count1 = 0;
            count2 = 0;
            for (auto it = nums.begin(); it != nums.end(); ++it) {
                if (*it == elm1) ++count1;
                if (*it == elm2) ++count2;
            }
            // Building the vector
            if (count1 > n/3) majority.push_back(elm1);
            if (count2 > n/3) majority.push_back(elm2);
            // Return majority
            return majority;
        }
};

int main() {
    Solution sol;
    vector<int> nums, majority, assertion;
    // Test cases
    // Test case 1
    assertion = {};
    nums      = {};
    std::cout << "Elements in the vector: ";
    for (int x : nums) std::cout << x << " ";
    std::cout << std::endl;
    majority = sol.majorityElement(nums);
    std::cout << "Majority element(s) is(are): ";
    for (int x : majority) std::cout << x << " ";
    std::cout << std::endl;
    assert(("Case 1: Empty array", majority == assertion));
    std::cout << std::endl;
    // Test case 2
    assertion = {1};
    nums      = {1};
    std::cout << "Elements in the vector: ";
    for (int x : nums) std::cout << x << " ";
    std::cout << std::endl;
    majority = sol.majorityElement(nums);
    std::cout << "Majority element(s) is(are): ";
    for (int x : majority) std::cout << x << " ";
    std::cout << std::endl;
    assert(("Case 2: One element", majority == assertion));
    std::cout << std::endl;
    // Test case 3
    assertion = {1,2};
    nums      = {1,2};
    std::cout << "Elements in the vector: ";
    for (int x : nums) std::cout << x << " ";
    std::cout << std::endl;
    majority = sol.majorityElement(nums);
    std::cout << "Majority element(s) is(are): ";
    for (int x : majority) std::cout << x << " ";
    std::cout << std::endl;
    assert(("Case 3: Distinct two elements", majority == assertion));
    std::cout << std::endl;
    // Test case 4
    assertion = {1};
    nums      = {1,1};
    std::cout << "Elements in the vector: ";
    for (int x : nums) std::cout << x << " ";
    std::cout << std::endl;
    majority = sol.majorityElement(nums);
    std::cout << "Majority element(s) is(are): ";
    for (int x : majority) std::cout << x << " ";
    std::cout << std::endl;
    assert(("Case 4: Identical two elements", majority == assertion));
    std::cout << std::endl;
    // Test case 5
    assertion = {3};
    nums      = {3,2,3};
    std::cout << "Elements in the vector: ";
    for (int x : nums) std::cout << x << " ";
    std::cout << std::endl;
    majority = sol.majorityElement(nums);
    std::cout << "Majority element(s) is(are): ";
    for (int x : majority) std::cout << x << " ";
    std::cout << std::endl;
    assert(("Case 5:", majority == assertion));
    std::cout << std::endl;
    // Test case 6
    assertion = {1,2};
    nums      = {1,1,1,3,3,2,2,2};
    std::cout << "Elements in the vector: ";
    for (int x : nums) std::cout << x << " ";
    std::cout << std::endl;
    majority = sol.majorityElement(nums);
    std::cout << "Majority element(s) is(are): ";
    for (int x : majority) std::cout << x << " ";
    std::cout << std::endl;
    assert(("Case 6: Empty array", majority == assertion));
    std::cout << std::endl;
    // Test case 7
    assertion = {};
    nums      = {1,2,3,4,5};
    std::cout << "Elements in the vector: ";
    for (int x : nums) std::cout << x << " ";
    std::cout << std::endl;
    majority = sol.majorityElement(nums);
    std::cout << "Majority element(s) is(are): ";
    for (int x : majority) std::cout << x << " ";
    std::cout << std::endl;
    assert(("Case 7: Degenerate, no majority", majority == assertion));
    std::cout << std::endl;
    // Compile successful
    return 0;
}
