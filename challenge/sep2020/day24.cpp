// Day 24, Find the Difference
#include <cassert>
#include <iostream>
#include <string>
using std::string;

class Solution {
    public:
        char findTheDifference(string s, string t) {
            char c = t.back();
            for (int i = 0; i < s.size(); ++i) c ^= s[i] ^ t[i];
            return c;
        }
};

int main() {
    // Declaring variables
    Solution sol;
    string s, t;
    char ans;
    // Test cases
    // Test case 1
    s = "";
    t = "a";
    assert(t.size() == s.size() + 1);
    std::cout << "Before: " << s << ", after: " << t << std::endl;
    // Finding answer
    ans = sol.findTheDifference(s,t);
    std::cout << "The answer is: " << ans << std::endl << std::endl;
    // Test case 2
    s = "b";
    t = "ab";
    assert(t.size() == s.size() + 1);
    std::cout << "Before: " << s << ", after: " << t << std::endl;
    // Finding answer
    ans = sol.findTheDifference(s,t);
    std::cout << "The answer is: " << ans << std::endl << std::endl;
    // Test case 3
    s = "b";
    t = "ba";
    assert(t.size() == s.size() + 1);
    std::cout << "Before: " << s << ", after: " << t << std::endl;
    // Finding answer
    ans = sol.findTheDifference(s,t);
    std::cout << "The answer is: " << ans << std::endl << std::endl;
    // Test case 4
    s = "abcd";
    t = "abcde";
    assert(t.size() == s.size() + 1);
    std::cout << "Before: " << s << ", after: " << t << std::endl;
    // Finding answer
    ans = sol.findTheDifference(s,t);
    std::cout << "The answer is: " << ans << std::endl << std::endl;
    // Compile successful
    return 0;
}
