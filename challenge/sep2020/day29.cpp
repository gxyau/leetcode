// Day 29, Word Break
#include <iostream>
#include <string>
#include <unordered_set>
#include <vector>
using std::string;
using std::vector;

class Solution {
    public:
        bool wordBreak(string s, vector<string>& wordDict) {
            std::unordered_set<string> set(wordDict.begin(), wordDict.end());
            const int n = s.size();
            vector<bool> DP(n+1, {false});
            DP[0] = true;
            for (int j = 1; j <= n; ++j) {
                for (int i = 0; i < j; ++i) {
                    if (DP[i] && set.count(s.substr(i,j-i))) {
                        DP[j] = true;
                        break;
                    }
                }
            }
            return DP[n];
        }
};

int main() {
    // Compile successful
    return false;
}
