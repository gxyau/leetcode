// Day 21, Car Pooling
#include <iostream>
#include <vector>
using std::vector;

class Solution {
    public:
        bool carPooling(vector<vector<int>>& trips, int capacity) {
            // Edge cases
            if (trips.empty()) {
                return true;
            } else if (capacity == 0) {
                return false;
            }
            // Variables needed for the problem
            int delta[1001] = {0}, onboard = 0, last = 0;
            // Finding the delta at each location
            for (auto trip : trips) {
                delta[trip[1]] += trip[0];
                delta[trip[2]] -= trip[0];
                last = std::max(last, trip[2]);
            }
            for (int i = 1; i < last; ++i) {
                onboard += delta[i];
                if (onboard > capacity) return false;
            }
            return true;
        }
};

int main() {
    Solution sol;
    vector<vector<int>> trips;
    int capacity;
    bool flag;
    // Test case 1
    trips    = {};
    capacity = 0;
    for (vector<int> trip : trips) {
        std::cout << "|";
        for (int x : trip) {
            std::cout << " " << x << " ";
        }
        std::cout << "|";
    }
    std::cout << std::endl;
    flag = sol.carPooling(trips, capacity);
    std::cout << "Car pooling successful? " << (flag ? "yes" : "no") << std::endl;
    // Test case 2
    trips    = {{2,1,5},{3,3,7}};
    capacity = 4;
    for (vector<int> trip : trips) {
        std::cout << "|";
        for (int x : trip) {
            std::cout << " " << x << " ";
        }
        std::cout << "|";
    }
    std::cout << std::endl;
    flag = sol.carPooling(trips, capacity);
    std::cout << "Car pooling successful? " << (flag ? "yes" : "no") << std::endl;
    // Test case 3
    trips    = {{2,1,5},{3,3,7}};
    capacity = 5;
    for (vector<int> trip : trips) {
        std::cout << "|";
        for (int x : trip) {
            std::cout << " " << x << " ";
        }
        std::cout << "|";
    }
    std::cout << std::endl;
    flag = sol.carPooling(trips, capacity);
    std::cout << "Car pooling successful? " << (flag ? "yes" : "no") << std::endl;
    // Test case 4
    trips    = {{2,1,5},{3,5,7}};
    capacity = 3;
    for (vector<int> trip : trips) {
        std::cout << "|";
        for (int x : trip) {
            std::cout << " " << x << " ";
        }
        std::cout << "|";
    }
    std::cout << std::endl;
    flag = sol.carPooling(trips, capacity);
    std::cout << "Car pooling successful? " << (flag ? "yes" : "no") << std::endl;
    // Test case 5
    trips    = {{3,2,7},{3,7,9},{8,3,9}};
    capacity = 11;
    for (vector<int> trip : trips) {
        std::cout << "|";
        for (int x : trip) {
            std::cout << " " << x << " ";
        }
        std::cout << "|";
    }
    std::cout << std::endl;
    flag = sol.carPooling(trips, capacity);
    std::cout << "Car pooling successful? " << (flag ? "yes" : "no") << std::endl;
    // Test case 6
    trips    = {{3,2,5},{3,5,9},{8,3,9}};
    capacity = 11;
    for (vector<int> trip : trips) {
        std::cout << "|";
        for (int x : trip) {
            std::cout << " " << x << " ";
        }
        std::cout << "|";
    }
    std::cout << std::endl;
    flag = sol.carPooling(trips, capacity);
    std::cout << "Car pooling successful? " << (flag ? "yes" : "no") << std::endl;
    // Test case 7
    trips    = {{3,1,9},{3,2,5},{8,5,9}};
    capacity = 11;
    for (vector<int> trip : trips) {
        std::cout << "|";
        for (int x : trip) {
            std::cout << " " << x << " ";
        }
        std::cout << "|";
    }
    std::cout << std::endl;
    flag = sol.carPooling(trips, capacity);
    std::cout << "Car pooling successful? " << (flag ? "yes" : "no") << std::endl;
    // Test case 8
    trips    = {{3,1,9},{3,2,5},{8,3,9}};
    capacity = 11;
    for (vector<int> trip : trips) {
        std::cout << "|";
        for (int x : trip) {
            std::cout << " " << x << " ";
        }
        std::cout << "|";
    }
    std::cout << std::endl;
    flag = sol.carPooling(trips, capacity);
    std::cout << "Car pooling successful? " << (flag ? "yes" : "no") << std::endl;
    // Compile successful?
    return 0;
}
