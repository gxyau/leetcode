// Smallest Integer Divisible by K
#include <iostream>

class Solution {
    public:
        int smallestRepunitDivByK(int K) {
            if (K%2 == 0) return -1;
            long r = 0;
            for (int i = 1; i <= K; ++i) {
                r = (r*10 + 1) % K;
                if (r == 0) return i;
            }
            return -1;
        }
};

int main() {
    Solution sol;
    int K, ans;
    // Small program
    std::cout << "Please enter an integer, K: ";
    std::cin  >> K;
    ans = sol.smallestRepunitDivByK(K);
    std::cout << "The answer is: " << ans << std::endl;
    return 0;
}
