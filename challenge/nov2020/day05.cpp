// Day 5, Minimum Cost to Move Chips to the Same Position
#include <vector>
using std::vector;

class Solution {
    public:
        int minCostToMoveChips(vector<int>& position) {
            if (position.size() == 1) return 0;
            int odd = 0, even = 0;
            for (int chip : position) {
                if (chip % 2 == 0) ++even;
                else ++odd;
            }
            return std::min(odd, even);
        }
};

int main() {
    // Compile successful
    return 0;
}
