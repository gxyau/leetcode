// Day 11, Valid Square
#include <cmath>
#include <set>
#include <vector>
using std::vector;

class Solution {
    private:
        double dist(vector<int>& p, vector<int>& q) {
            return std::sqrt((p[0] - q[0]) * (p[0] - q[0]) + (p[1] - q[1]) * (p[1] - q[1]));
        }
    public:
        bool validSquare(vector<int>& p1, vector<int>& p2, vector<int>& p3, vector<int>& p4) {
            vector<vector<int>> points = {p1, p2, p3, p4};
            set<double> distances;
            // Computing all distances
            for (int i = 0; i < 4; ++i) {
                for (int j = i+1; j < 4; ++j) {
                    double d = dist(points[i], points[j]);
                    if (d != 0) distances.insert(d);
                    else return false;
                }
            }
            // Should only contain sides and diagonals
            return distances.size() == 2;
        }
};

int main() {
    // Compile successful
    return 0;
}
