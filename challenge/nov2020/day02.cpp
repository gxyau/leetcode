// Day 2, Insertion Sort List
#include <algorithm>

// Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
    public:
        ListNode* insertionSortList(ListNode* head) {
            if (!head) return head;
            ListNode *cur, *it, *pos = head;
            while (pos->next) {
                if (pos->val <= pos->next->val) {
                    pos = pos->next;
                    continue;
                }
                cur       = pos->next;
                pos->next = pos->next->next;
                if (head->val > cur->val) {
                    cur->next = head;
                    head      = cur;
                } else {
                    it = head;
                    while (it->next->val <= cur->val) it = it->next;
                    cur->next = it->next;
                    it->next  = cur;
                }
            }
            return head;
        }
};

int main() {
    // Compile successful
    return 0;
}
