// Day 26, Longest Substring with At Least K Repeating Characters
#include <string>
using std::string;

class Solution {
    public:
        int longestSubstring(string s, int k) { 
            int max_len = 0;
            for (int length = 1; length <= 26; length++) {
                int counter[26] = {0}, unique = 0, satisfied = 0;
                string::iterator left = s.begin(), right = s.begin();
                while (right != s.end()) {
                    ++counter[*right-'a'];
                    if (counter[*right-'a'] == 1) ++unique;
                    if (counter[*right-'a'] == k) ++satisfied;
                    while (unique > length) {
                        --counter[*left-'a'];
                        if (counter[*left-'a'] == k-1) --satisfied;
                        if (counter[*left-'a'] == 0) --unique;
                        left++;
                    }
                    int cur = (++right)-left;
                    if (unique == satisfied)
                        max_len = std::max(max_len, cur);
                }
            }
            return max_len;
        }
};

int main() {
    // Compile successful
    return 0;
}
