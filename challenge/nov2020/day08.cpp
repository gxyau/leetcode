// Day 8, Binary Tree Tilt
#include <cstdlib>

// Definition for a binary tree node.
struct TreeNode {
   int val;
   TreeNode *left;
   TreeNode *right;
   TreeNode() : val(0), left(nullptr), right(nullptr) {}
   TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
   TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
    private:
        int tilt;
        int subtree_sum(TreeNode* root) {
            if (!root) return 0;
            int left = subtree_sum(root->left), right = subtree_sum(root->right);
            tilt += std::abs(left - right);
            return root->val + left + right;
        }
    public:
        int findTilt(TreeNode* root) {
            tilt = 0;
            subtree_sum(root);
            return tilt;
        }
};

int main() {
    // Compile successful
    return 0;
}
