// Day 29, Jump Game III
#include <vector>
using std::vector;

class Solution {
    private:
        void reachable_squares(vector<int>& arr, vector<bool>& reach, int start) {
            int n = arr.size();
            // If we visited this square before we do not continue
            if (reach[start]) return;
            else reach[start] = true;
            // If within range we visit the next square
            if (start + arr[start] < n) reachable_squares(arr, reach, start+arr[start]);
            if (start >= arr[start]) reachable_squares(arr, reach, start-arr[start]);
            return;
        }
    public:
        bool canReach(vector<int>& arr, int start) {
            vector<bool> reach(arr.size(), {false});
            reachable_squares(arr, reach, start);
            for (int i = 0; i < arr.size(); ++i) 
                if (reach[i] && (arr[i] == 0)) return true;
            return false;
        }
};

int main() {
    // Compile successful
    return 0;
}
