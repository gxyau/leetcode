// Day 3, Consecutive Characters
#include <string>
using std::string;

class Solution {
    public:
        int maxPower(string s) {
            int maximum = 1, power = 1;
            char prev = ' ';
            for (char c : s) {
                if (c == prev) {
                    ++power;
                } else {
                    power   = 1;
                    prev    = c;
                }
                maximum = std::max(maximum, power);
            }
            return maximum;
        }
};

int main() {
    // Compile successful
    return 0;
}
