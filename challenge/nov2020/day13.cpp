// Day 13, Populating Next Right Pointers in Each Node
#include <queue>
using std::queue;

// Definition for a Node.
class Node {
    public:
        int val;
        Node* left;
        Node* right;
        Node* next;

        Node() : val(0), left(NULL), right(NULL), next(NULL) {}

        Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}

        Node(int _val, Node* _left, Node* _right, Node* _next)
            : val(_val), left(_left), right(_right), next(_next) {}
};

class Solution {
    public:
        Node* connect(Node* root) {
            if (!root) return root;
            queue<Node*> q;
            q.push(root);
            while(!q.empty()) {
                int size = q.size();
                // Previous node
                Node *prev = nullptr;
                while (size--) {
                    // Get current node
                    Node *cur = q.front();
                    q.pop();
                    if (cur->left) q.push(cur->left);
                    if (cur->right) q.push(cur->right);
                    if (prev) prev->next = cur;
                    prev = cur;
                }
            }
            return root;
        }
};

int main() {
    // Compile successful
    return 0;
}
