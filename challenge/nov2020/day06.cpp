// Day 6, Find the Smallest Divisor Given a Threshold
#include <limits>
#include <vector>
using std::vector;

class Solution {
    public:
        int smallestDivisor(vector<int>& a, int threshold) {
            long long int l = 1,r = 1000000, ans = std::numeric_limits<int>::max();
            int n = a.size();
            while(l <= r){
                long long int m = l + (r-l)/2;
                long long int sum = 0;
                for(int i = 0; i < n; ++i) sum += (a[i]+m-1)/m;
                if (sum <= threshold) ans = min(ans,m), r = m-1;
                else l = m+1;
            }
            return ans;
        }
};

int main() {
    // Compile successful
    return 0;
}
