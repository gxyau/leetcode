// Day 10, Flipping an Image
#include <algorithm>
#include <vector>
using std::vector;

class Solution {
    public:
        vector<vector<int>> flipAndInvertImage(vector<vector<int>>& A) {
            vector<vector<int>> flip = {};
            for (auto row : A) {
                vector<int> flipped_row = {};
                for (auto it = row.rbegin(); it != row.rend(); ++it) {
                    flipped_row.push_back(1-*it);
                }
                flip.push_back(flipped_row);
            }
            return flip;
        }
};

int main() {
    // Compiles successful
    return 0;
}
