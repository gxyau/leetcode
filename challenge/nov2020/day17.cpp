// Day 17, Mirror Reflection
// MUST COMPILE USING C++17
#include <numeric>

class Solution {
    public:
        int mirrorReflection(int p, int q) {
            int d = std::gcd(p,q);
            p /= d, q /= d;
            return (p%2) ? (q%2) : 2;
        }
};

int main() {
    // Compile successful
    return 0;
}
