// Day 28, Sliding Window Maximum
#include <deque>
#include <vector>
using std::vector;

class Solution {
    public:
        vector<int> maxSlidingWindow(vector<int>& nums, int k) {
            vector<int> window;
            std::deque<int> dq;
            for(int i = 0;i < nums.size(); ++i){
                if(!dq.empty() && i-k == dq.front()) dq.pop_front();
                while(!dq.empty() && nums[i] > nums[dq.back()]) dq.pop_back();
                dq.push_back(i);
                if(i>=k-1) window.push_back(nums[dq.front()]);
            }
            return window;
        }
};

int main() {
    // Compile successful
    return 0;
}
