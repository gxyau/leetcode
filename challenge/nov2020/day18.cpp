// Day 18, Merging Intervals
#include <vector>
using std::vector;

class Solution {
    public:
        vector<vector<int>> merge(vector<vector<int>>& intervals) {
            sort(intervals.begin(), intervals.end());
            vector<vector<int>> ans;
            // Merging intersecting intervals
            for(int i = 0, n = intervals.size(); i < n; ++i) {
                int start = intervals[i][0];
                int end   = intervals[i][1];
                while(i < n-1 && end >= intervals[i+1][0]) end = max(end, intervals[++i][1]);
                ans.push_back({start, end});            
            }
            // Return answer
            return ans;
        }
};

int main() {
    // Compile successful
    return 0;
}
