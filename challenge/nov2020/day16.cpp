// Day 16, Longest Mountain in an Array
#include <iostream>
#include <vector>
using std::vector;

class Solution {
    public:
        int longestMountain(vector<int>& A) {
            int s = 0, p = -1, res = 0, n = A.size();
            for (int i = 1; i < n; i++) {
                if (p != -1) {
                    if (A[i] < A[i-1]) res = max(i-s+1, res);
                    else p = -1;
                }
                if ((i < n-1 && A[i] < A[i+1]) && A[i] <= A[i-1]) {
                    s = i;
                }
                if ((A[i-1] < A[i]) && (i < n-1 && A[i] > A[i+1])) {
                    p = i;
                }            
            }
            return res;
        }
};

int main() {
    Solution sol;
    vector<int> A = {};
    int length;
    // Test cases
    // Test case 1, Base case empty array
    length = sol.longestMountain(A);
    std::cout << "The array is: ";
    for (int x : A) std::cout << x << " ";
    std::cout << std::endl; 
    std::cout << "Expected length 0, output: " << length << std::endl;
    std::cout << std::endl; 
    // Test case 2, Array of length 1
    A = {1};
    length = sol.longestMountain(A);
    std::cout << "The array is: ";
    for (int x : A) std::cout << x << " ";
    std::cout << std::endl; 
    std::cout << "Expected length 0, output: " << length << std::endl;
    std::cout << std::endl; 
    // Test case 3, Array of length 2
    A = {1,2};
    length = sol.longestMountain(A);
    std::cout << "The array is: ";
    for (int x : A) std::cout << x << " ";
    std::cout << std::endl; 
    std::cout << "Expected length 0, output: " << length << std::endl;
    std::cout << std::endl; 
    // Test case 4, Increasing sequence
    A = {1,2,3};
    length = sol.longestMountain(A);
    std::cout << "The array is: ";
    for (int x : A) std::cout << x << " ";
    std::cout << std::endl; 
    std::cout << "Expected length 0, output: " << length << std::endl;
    std::cout << std::endl; 
    // Test case 5, Decreasing sequence
    A = {3,2,1};
    length = sol.longestMountain(A);
    std::cout << "The array is: ";
    for (int x : A) std::cout << x << " ";
    std::cout << std::endl; 
    std::cout << "Expected length 0, output: " << length << std::endl;
    std::cout << std::endl; 
    // Test case 6, Same integers
    A = {2,2,2};
    length = sol.longestMountain(A);
    std::cout << "The array is: ";
    for (int x : A) std::cout << x << " ";
    std::cout << std::endl; 
    std::cout << "Expected length 0, output: " << length << std::endl;
    std::cout << std::endl; 
    // Test case 7, One mountain
    A = {1,3,2};
    length = sol.longestMountain(A);
    std::cout << "The array is: ";
    for (int x : A) std::cout << x << " ";
    std::cout << std::endl; 
    std::cout << "Expected length 3, output: " << length << std::endl;
    std::cout << std::endl; 
    // Test case 8, Larger array, no mountain
    A = {1,3,3,2};
    length = sol.longestMountain(A);
    std::cout << "The array is: ";
    for (int x : A) std::cout << x << " ";
    std::cout << std::endl; 
    std::cout << "Expected length 0, output: " << length << std::endl;
    std::cout << std::endl; 
    // Test case 9, Larger array, one mountain
    A = {1,3,2,2};
    length = sol.longestMountain(A);
    std::cout << "The array is: ";
    for (int x : A) std::cout << x << " ";
    std::cout << std::endl; 
    std::cout << "Expected length 3, output: " << length << std::endl;
    std::cout << std::endl; 
    // Test case 10, Generic case
    A = {2,1,4,7,3,2,5};
    length = sol.longestMountain(A);
    std::cout << "The array is: ";
    for (int x : A) std::cout << x << " ";
    std::cout << std::endl; 
    std::cout << "Expected length 5, output: " << length << std::endl;
    std::cout << std::endl; 
    // Test case 11, Generic case (reversed)
    A = {5,2,3,7,4,1,2};
    length = sol.longestMountain(A);
    std::cout << "The array is: ";
    for (int x : A) std::cout << x << " ";
    std::cout << std::endl; 
    std::cout << "Expected length 5, output: " << length << std::endl;
    std::cout << std::endl; 
    // Compile successful
    return 0;
}
