// Day 19, Decode String
#include <string>
using std::string;

class Solution {
    public:
        string decodeString(string s) {
            while(!s.empty()){
                int pos1=-1,pos2=-1,posk=0;
                for(;posk<s.size();posk++) if(!(s[posk]>='0'&&s[posk]<='9')) break;
                for(int i=posk;i<s.size();i++){
                    if(s[i] == '[') pos1 = i;
                    if(s[i] == ']') {pos2 = i;break;}
                }
                int pos3 = pos1-1, k = 0, n = 1;
                for(;pos3>=0;pos3--){
                    int t = s[pos3] - '0';
                    if(t >= 0 &&  t < 10) {
                        k  = k + t*n;
                        n *= 10;
                    } else {
                        break;
                    }
                }
                if(!k) k=1;
                string ret,st="";
                if(pos2 != -1) ret = s.substr(pos1+1,pos2 - pos1 - 1);
                else break;
                while(k--) st += ret;
                s.erase(pos3 +1 ,pos2-pos3);
                s.insert(pos3 +1,st);
            }
            return s;
        }
};

int main() {
    // Compile successful
    return 0;
}
