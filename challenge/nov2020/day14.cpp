// Day 14, Poor Pigs
#include <cmath>

class Solution {
    public:
        int poorPigs(int buckets, int minutesToDie, int minutesToTest) {
            return std::ceil(std::log(buckets) / std::log(minutesToTest / minutesToDie + 1));
        }
};

int main() {
    // Compile successful
    return 0;
}
