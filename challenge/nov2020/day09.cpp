// Day 9, Maximum Difference Between Node and Ancestor
#include <algorithm>

// Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
    private:
        int rec(TreeNode* root, int minimum, int maximum) {
            if (!root) return maximum - minimum;
            minimum = std::min(minimum, root->val);
            maximum = std::max(maximum, root->val);
            int l   = rec(root->left, minimum, maximum);
            int r   = rec(root->right, minimum, maximum);
            return std::max(l, r);
        }
    public:
        int maxAncestorDiff(TreeNode* root) {
            return rec(root, root->val, root->val);
        }
};

int main() {
    // Compile successful
    return 0;
}
