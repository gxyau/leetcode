// Day 27, Partition Equal Subset Sum
#include <iostream>
#include <string>
#include <vector>
using std::string;
using std::vector;

class Solution {
    public:
        bool canPartition(vector<int>& nums) {
            int sum = std::accumulate(begin(nums), end(nums), 0);
            if (sum % 2) return false;
            sum /= 2;
            bitset<10001> dp = 1;
            for (int n : nums) {
                dp |= dp << n;
            }
            return dp[sum];
        }
};

int main() {
    Solution sol;
    vector<int> nums = {};
    string flag;
    // Test cases
    // Test case 1
    nums = {1,5,11,5};
    flag = sol.canPartition(nums) ? "True" : "False";
    std::cout << "Flag: " << flag << std::endl;
    // Compile successful
    return 0;
}
