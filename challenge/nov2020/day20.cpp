// Day 20, Search in Rotated Sorted Array
#include <vector>
using std::vector;

class Solution {
    public:
        bool search(vector<int>& nums, int target) {
            for (int x : nums) {
                if (x == target) return true;
            }
            return false;
        }
};

int main() {
    // Compile successful
    return 0;
}
