// Day 7, Add Two Numbers II
// Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
    private:
        ListNode *ans;
        bool carry = false;
        void sum(ListNode* l1, ListNode* l2) {
            if (!l1 && !l2) return;
            ListNode *ptr = new ListNode(0);
            sum(l1->next, l2->next);
            int s = 0;
            s += l1->val + l2->val;
            if (carry) ++s;
            carry = s > 9;
            ptr->val  = s % 10;
            ptr->next = ans;
            ans = ptr;
        }
    public:
        ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
            ListNode *ptr;
            int len1 = 0, len2 = 0;
            // Getting lengths
            ptr = l1;
            while(ptr) {++len1; ptr = ptr->next;}
            ptr = l2;
            while(ptr) {++len2; ptr = ptr->next;}
            // Making both lists same length
            while (len1 < len2) {
                ptr = new ListNode(0);
                ptr->next = l1;
                l1 = ptr;
                ++len1;
            }
            while (len2 < len1) {
                ptr = new ListNode(0);
                ptr->next = l2;
                l2 = ptr;
                ++len2;
            }
            sum(l1,l2);
            if (carry) {
                ListNode *ptr = new ListNode(1);
                ptr->next = ans;
                ans = ptr;
            }
            return ans;
        }
};
};

int main() {
    // Compile successful
    return 0;
}
