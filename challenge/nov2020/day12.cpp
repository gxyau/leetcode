// Day 12, Permutation II
#include <algorithm>
#include <vector>
using std::vector;

class Solution {
    public:    
        void nextPermutation(vector<int>& nums) {
            int n = nums.size()-1;
            while(n > 0 && nums[n] <= nums[n-1]) --n;
            if (n == 0) {
                std::reverse(nums.begin(), nums.end());
            } else {
                int m = n-1;
                while(n < nums.size() - 1 && nums[n+1] > nums[m]) ++n;
                std::swap(nums[m], nums[n]);
                reverse(nums.begin()+m+1,nums.end());
            }
        }

        vector<vector<int>> permuteUnique(vector<int>& nums) {
            vector<vector<int>> permutations;
            vector<int> initial = nums;
            while (true) {
                permutations.push_back(nums);
                nextPermutation(nums);
                if (nums == initial) break;
            }
            return permutations;
        }
};

int main() {
    // Compile successful
    return 0;
}
