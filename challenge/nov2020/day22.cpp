// Day 22, Unique Morse Code Words
#include <string>
#include <unordered_set>
#include <vector>
using std::string;
using std::vector;

class Solution {
    private:
        const string MORSE[26] = {".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."};
    public:
        int uniqueMorseRepresentations(vector<string>& words) {
            std::unordered_set<string> unique = {};
            for (string w : words) {
                string s = "";
                for (char c : w) {
                    s += MORSE[c - 'a'];
                }
                unique.insert(s);
            }
            return unique.size();
        }
};

int main() {
    // Compile successful
    return 0;
}
