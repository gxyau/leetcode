// Numbers At Most N Given Digit Set
#include <cmath>
#include <string>
#include <vector>
using std::string;
using std::vector;

class Solution {
    private:
        static int count_combinations(vector<string>& d, int n, int k) {
            if (k <= 0) return 1;
            int ans = 0;
            int p = std::pow(10, k - 1);
            for (int i = 0; i < d.size(); ++i) {
                const int digit = d[i][0] - '0';
                const int num = p * (digit + 1) - 1;
                if (num <= n) {
                    ans += std::pow(d.size(), k - 1);
                } else if (p * digit < n) {
                    ans += count_combinations(d, n % p, k - 1);
                } else {
                    break;
                }
            }
            return ans;
        }

        static constexpr int magnitude(int n) {
            int ans = 0;
            while (n) {
                ++ans;
                n /= 10;
            }
            return ans;
        }
    public:
        static int atMostNGivenDigitSet(vector<string>& d, int n) {
            int ans = 0, k = magnitude(n);
            while(k) ans += count_combinations(d, n, k--);
            return ans;
        }
};

int main() {
    // Compile successful
    return 0;
}
