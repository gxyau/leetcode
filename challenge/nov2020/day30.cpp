// Day 30, The Skyline Problem
#include <map>
#include <vector>
using std::vector;

class Solution {
    public:
        vector<vector<int>> getSkyline(vector<vector<int>>& buildings) {
            std::map<int, vector<int>> M;
            for (auto b : buildings) {
                // Left height
                M[b[0]].push_back(b[2]);
                // Right height
                M[b[1]].push_back(-b[2]);
            }
            vector<vector<int>> skyline = {};
            map<int, int> counts;
            int prev = 0;
            for (auto it = M.begin(); it != M.end(); ++it) {
                for (int x : it->second) {
                    if (x<0) {
                        --counts[-x];
                        if (counts[-x]==0)
                            counts.erase(-x);
                    } else {
                        ++counts[x];
                    }
                }
                int curr;
                if (counts.size()==0)
                    curr = 0;
                else
                    curr = counts.rbegin()->first;
                if (curr != prev) {
                    vector<int> point = {it->first, curr};
                    skyline.push_back(point);
                }
                prev = curr;
            }
            return skyline;
        }
};

int main() {
    // Compile successful
    return 0;
}
