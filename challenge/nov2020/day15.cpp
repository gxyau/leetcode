// Day 15, Range Sum of BST

// Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
    private:
        int sum, lb, ub;
        void add_value(TreeNode* root) {
            if (!root) {
                return;
            } else if (root->val < lb) {
                add_value(root->right);
                return;
            } else if (root->val > ub) {
                add_value(root->left);
                return;
            }
            sum += root->val;
            add_value(root->left);
            add_value(root->right);
        }
    public:
        int rangeSumBST(TreeNode* root, int low, int high) {
            sum = 0, lb  = low, ub = high;
            add_value(root);
            return sum;
        }
};

int main() {
    // Compile successful
    return 0;
}
