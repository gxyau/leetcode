// Day 24, Basic Calculator II
#include <string>
using std::string;

class Solution {
    public:
        int calculate(string s) {
            if (s.empty()) return 0;
            int n = s.length();
            int curr = 0, prev = 0, result = 0;
            char sign = '+';
            for (int i = 0; i < n; i++) {
                char c = s[i];
                if (isdigit(c)) {
                    curr = (curr * 10) + (c - '0');
                }
                if (!isdigit(c) && !iswspace(c) || i == n - 1) {
                    if (sign == '+' || sign == '-') {
                        result += prev;
                        prev = (sign == '+') ? curr : -curr;
                    } else if (sign == '*') {
                        prev = prev * curr;
                    } else if (sign == '/') {
                        prev = prev / curr;
                    }
                    sign = c;
                    curr = 0;
                }
            }
            result += prev;
            return result;  
        }
};

int main() {
    // Compile successful
    return 0;
}
