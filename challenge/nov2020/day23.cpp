// Day 23, House Robber III
#include <algorithm>
#include <utility>

// Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
    private:
        std::pair<int, int> DFS(TreeNode* root) {
            if (!root) return {0,0};
            std::pair<int, int> l = DFS(root->left), r = DFS(root->right);
            int val1 = l.first + r.first, val2 = l.second + r.second;
            return{std::max(val2+root->val, val1), val1};
        }
    public:
        int rob(TreeNode* root) {
            if (!root) return 0;
            return DFS(root).first;
        }
};

int main() {
    // Compile successful
    return 0;
}
