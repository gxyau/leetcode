// Day 11, Remove Duplicates from Sorted Array II
#include <vector>
using std::vector;

class Solution {
    public:
        int removeDuplicates(vector<int>& nums) {
            if (nums.size() <= 2) return nums.size();
            for (auto it = nums.begin(); it < nums.end()-2; ++it) {
                while (*it == *(it+2) && it < nums.end()-2) nums.erase(it+2);
            }
            return nums.size();
        }
};

int main() {
    // Exit successful
    return 0;
}
