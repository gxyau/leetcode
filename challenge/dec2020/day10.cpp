// Day 10, Valid Mountain Array
#include <vector>
using std::vector;

class Solution {
    public:
        bool validMountainArray(vector<int>& arr) {
            if (arr.size() <= 2) return false;
            int i = 0;
            // Find the peak
            while (i < arr.size()-1 && arr[i] < arr[i+1]) ++i;  
            // Peak can't be the first or last:
            if (i == 0 || i == arr.size()-1) return false;
            // Downhill
            while (i < arr.size()-1 && arr[i] > arr[i+1]) i++;
            // If there's a larger number after we started walking down, this will be false:
            return i == arr.size()-1;
        }
};

int main() {
    // Exit successful
    return 0;
}
