// Day 7, Spiral Matrix II
#include <cassert>
#include <iostream>
#include <vector>
using std::vector;

class Solution {
    public:
        vector<vector<int>> generateMatrix(int n) {
            vector<vector<int>> grid(n, vector<int>(n, {0}));
            int num = 0, d = 0, x = 0, y = -1, len = n;
            vector<std::pair<int, int>> directions = {{0,1}, {1,0}, {0,-1}, {-1,0}};
            while (num < n*n) {
                for (int  i = 0; i < len; ++i) {
                    x += directions[d].first;
                    y += directions[d].second;
                    grid[x][y] = ++num;
                }
                (++d) %= 4;
                if (d%2) --len;
            }
            return grid;
        }
};

int main() {
    // Variables
    Solution sol;
    int n;
    // Mini program
    std::cout << "Please enter a number (1-20): ";
    std::cin  >> n;
    assert(0 < n && n < 21);
    vector<vector<int>> grid = sol.generateMatrix(n);
    std::cout << "The matrix looks like:" << std::endl;
    for (auto vec : grid) {
        for (int x : vec) {
            std::cout << x << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
    // Exit successful
    return 0;
}
