// Day 3, Increasing Order Search Tree

// Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
    private:
        TreeNode *head, *cur;
    public:
        TreeNode* increasingBST(TreeNode* root) {
            if (!root) return nullptr;
            increasingBST(root->left);
            if (!head) {
                head = new TreeNode();
                cur  = head;
            }
            root->left = nullptr;
            cur->right = root;
            cur        = cur->right;
            increasingBST(root->right);
            return head->right;
        }
};

int main() {
    // Return successful
    return 0;
}
