// Day 6, Populating Next Right Pointers in Each Node II
#include <vector>
using std::vector;

// Definition for a Node.
class Node {
    public:
        int val;
        Node* left;
        Node* right;
        Node* next;
        
        Node() : val(0), left(NULL), right(NULL), next(NULL) {}
        
        Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}
        
        Node(int _val, Node* _left, Node* _right, Node* _next)
            : val(_val), left(_left), right(_right), next(_next) {}
};

class Solution {
    vector<vector<Node *> > vec;
    void dfs(Node *root, int level) {
        if(not root)    return;
        if(vec.size() <= level) vec.push_back({});
        if(vec[level].size() > 0)
            vec[level][vec[level].size()-1]->next = root;
        vec[level].push_back(root);
        dfs(root->left, level + 1);
        dfs(root->right, level + 1);
    }
public:
    Node* connect(Node* root) {
        dfs(root, 0);
        return root;
    }
};

int main() {
    // Return successful
    return 0;
}
