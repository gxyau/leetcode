// Day 8, Pairs of Songs With Total Durations Divisible by 60
#include <vector>
using std::vector;

class Solution {
    public:
        int numPairsDivisibleBy60(vector<int>& time) {
            vector<int> len(60, {0});
            int count = 0;
            for (int t : time) {
                if (t%60) {
                    count += len[60-(t%60)];
                } else {
                    count += len[0];
                }
                ++len[t % 60];
            }
            return count;
        }
};

int main() {
    // Exit successful
    return 0;
}
