// Day 1, Maximum Depth of Binary Tree

 // Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
    private:
        int maximum;
        void depth(TreeNode* root, int level) {
            if (!root) return;
            maximum = std::max(maximum, level+1);
            depth(root->left, level+1);
            depth(root->right, level+1);
            return;
        }
    public:
        int maxDepth(TreeNode* root) {
            maximum = 0;
            depth(root, 0);
            return maximum;
        }
};

int main() {
    // Return successful
    return 0;
}
