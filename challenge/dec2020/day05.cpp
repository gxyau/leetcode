// Day 5, Can Place Flowers
#include <vector>
using std::vector;

class Solution {
    public:
        bool canPlaceFlowers(vector<int>& flowerbed, int n) {
            int count = 0, k = 0, m = flowerbed.size();
            while (k < m) {
                if (flowerbed[k] == 0 && (k == 0 || flowerbed[k-1] == 0) && (k == m-1 || flowerbed[k+1] == 0)) {
                    ++count;
                    flowerbed[k++] = 1;
                }
                if (count >= n) return true;
                ++k;
            }
            return false;
        }
};
1


int main() {
    // Return successful
    return 0;
}
