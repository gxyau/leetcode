// Day 4, The kth Factor of n
#include <algorithm>
#include <cmath>
#include <vector>
using std::vector;

class Solution {
    public:
        int kthFactor(int n, int k) {
            vector<int> factors = {1, n};
            for (int i = 2; i <= ((int) std::sqrt(n)); ++i) {
                if (n % i == 0) {
                    auto it = std::upper_bound(factors.begin(), factors.end(), i);
                    factors.insert(it, i);
                    if (n/i != i) {
                        it = std::upper_bound(factors.begin(), factors.end(), n/i);
                        factors.insert(it, n/i);
                    }
                }
            }
            return (factors.size() >= k) ? factors[k-1] : -1;
        }
};

int main() {
    // Return successful
    return 0;
}
