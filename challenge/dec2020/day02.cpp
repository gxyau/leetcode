// Linked List Random Node
#include <random>

// Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
    private:
        ListNode *head;
    public:
        /** @param head The linked list's head.
            Note that the head is guaranteed to be not null, so it contains at least one node. */
        Solution(ListNode* head) {
            this->head = head;
        }
        
        /** Returns a random node's value. */
        int getRandom() {
            // Pointer and the sample
            ListNode *ptr = head, *sample;
            int n = 1;
            // Random number generator
            std::random_device rd;  //Will be used to obtain a seed for the random number engine
            std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
            std::uniform_real_distribution<> dis(0.0, 1.0);
            while (ptr) {
                double prob = dis(gen);
                if (prob <= (double) 1/n++) sample = ptr;
                ptr = ptr->next;
            }
            return sample->val;
        }
};

int main() {
    // Return successful
    return 0;
}

/**
 * Your Solution object will be instantiated and called as such:
 * Solution* obj = new Solution(head);
 * int param_1 = obj->getRandom();
 */
