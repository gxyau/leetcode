// Day 8, Binary Search
#include <iostream>
#include <vector>
using std::vector;

class Solution {
    public:
        int search(vector<int>& nums, int target) {
            if (nums.empty()) return -1;
            const int n = nums.size();
            int l = 0, r = n-1, m;
            while (l<=r) {
                m = l + (r-l)/2;
                if (nums[m] < target) {
                    l = m+1;
                } else if (nums[m] > target) {
                    r = m-1;
                } else {
                    index = m;
                    break;
                }
            }
            return -1;
        }
};

int main() {
    Solution sol;
    vector<int> nums;
    int target, index;
    // Test cases
    // Test case 1
    nums   = {};
    target = -1;
    std::cout << "The vector is: ";
    for (int x : nums) std::cout << x << " ";
    std::cout << std::endl;
    std::cout << "Target: " << target << std::endl;
    index  = sol.search(nums, target);
    std::cout << "Target is at position " << index << std::endl;
    // Test case 2
    nums   = {1};
    target = 0;
    std::cout << "The vector is: ";
    for (int x : nums) std::cout << x << " ";
    std::cout << std::endl;
    std::cout << "Target: " << target << std::endl;
    index  = sol.search(nums, target);
    std::cout << "Target is at position " << index << std::endl;
    // Test case 3
    nums   = {1};
    target = 1;
    std::cout << "The vector is: ";
    for (int x : nums) std::cout << x << " ";
    std::cout << std::endl;
    std::cout << "Target: " << target << std::endl;
    index  = sol.search(nums, target);
    std::cout << "Target is at position " << index << std::endl;
    // Test case 4
    nums   = {-1,0,3,5,9,12};
    target = 9;
    std::cout << "The vector is: ";
    for (int x : nums) std::cout << x << " ";
    std::cout << std::endl;
    std::cout << "Target: " << target << std::endl;
    index  = sol.search(nums, target);
    std::cout << "Target is at position " << index << std::endl;
    // Test case 1
    nums   = {-1,0,3,5,9,12};
    target = 2;
    std::cout << "The vector is: ";
    for (int x : nums) std::cout << x << " ";
    std::cout << std::endl;
    std::cout << "Target: " << target << std::endl;
    index  = sol.search(nums, target);
    std::cout << "Target is at position " << index << std::endl;
    // Compile successful
    return 0;
}
