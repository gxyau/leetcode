// Day 14, House Robber II
#include <iostream>
#include <vector>
using std::vector;

class Solution {
    public:
        int rob(vector<int>& nums) {
            // Edge cases
           if (nums.size() == 1) return nums[0];
            // Prep for DP
            const int N = nums.size();
            int a0 = 0, a1 = nums[0], b0 = 0, b1 = 0;
            for (int i = 1; i < N-1; ++i) {
                std::swap(a0,a1);
                a1 = max(a1 + nums[i], a0);
                std::swap(b0,b1);
                b1 = max(b1 + nums[i], b0);
            }
            b1 = std::max(b0+nums[N-1], b1);
            return max(a1, b1);
        }
};

int main() {
    // Compile successful
    return 0;
}
