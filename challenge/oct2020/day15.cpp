// Day 15, Rotate Array
#include <algorithm>
#include <vector>
using std::vector;

class Solution {
    public:
        void rotate(vector<int>& nums, int k) {
            k %= nums.size();
            std::rotate(nums.rbegin(), nums.rbegin()+k, nums.rend());
        }
};

int main() {
    // Compile successful
    return 0;
}
