// Day 30, Number of Longest Increasing Subsequence
#include <pair>
#include <vector>
using std::vector;

class Solution {
    public:
        int findNumberOfLIS(vector<int>& nums) {
            if (nums.size() <= 1) return nums.size();
            const int n = nums.size();
            vector<std::pair<int, int>> dp(n, {1,1});
            int res = 1;
            for(int i = 1; i < nums.size(); i++) {
                for(int j = 0; j < i; j++) {
                    if(nums[i] > nums[j]) {
                        if( dp[j].first + 1 > dp[i].first) {
                            dp[i].first = dp[j].first + 1;
                            dp[i].second = dp[j].second;
                        }
                        else if (dp[j].first + 1 == dp[i].first) {
                            dp[i].second += dp[j].second;
                        }
                    }
                    res = max(res, dp[i].first);
                }
            }
            int count = 0;
            for(auto &pair : dp) {
                if(res == pair.first) count += pair.second;
            }
            return count;
        }
};

int main() {
    // Compile successful
    return 0;
}
