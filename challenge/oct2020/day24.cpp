// Day 24, Bag of Tokens
#include <algorithm>
#include <vector>
using std::vector;

class Solution {
    public:
        int bagOfTokensScore(vector<int>& tokens, int P) {
            std::sort(tokens.begin(),tokens.end());
            int l = 0, r = tokens.size()-1;
            int score = 0,ans = 0;
            while(l <= r && (P >= tokens[l] || score > 0) ){
                while(l <= r && P >= tokens[l]){
                    P -= tokens[l++];
                    ++score;
                }
                ans = std::max(ans,score);
                if(l <= r && score > 0){
                    P += tokens[r--];
                    --score;
                }
            }
            return ans;
        }
};

int main() {
    // Compile successful
    return 0;
}
