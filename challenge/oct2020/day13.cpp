// Day 13, Sort List
#include <iostream>

// Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
    private:
        void merge(ListNode** head, size_t size){
            if(size==0){
                *head = nullptr;
                return;
            }
            else if(size==1){
                (*head)->next=nullptr;
                return;
            }

            ListNode* pl = *head;
            ListNode* pr = pl;
            size_t lsize = size/2;
            size_t rsize = size-lsize;

            size_t cnt=lsize;
            while(cnt--){
                pr = pr->next;
            }

            merge(&pl, lsize);
            merge(&pr, rsize);

            while(pl && pr){
                if(pl->val < pr->val){
                    *head = pl;
                    pl = pl->next;
                }
                else{
                    *head = pr;
                    pr = pr->next;
                }

                head = &((*head)->next);
            }

            if(pl || pr) *head = pl?pl:pr;

            return;
        }
    public:
        ListNode* sortList(ListNode* head) {
            size_t cnt=0;
            ListNode* p = head;
            while(p){
                cnt++;
                p = p->next;
            }

            merge(&head, cnt);
            return head;
        }
};

int main() {
    // Compile successful
    return 0;
}
