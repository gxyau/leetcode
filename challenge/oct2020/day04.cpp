// Day 4, Remove Covered Intervals
#include <algorithm>
#include <iostream>
#include <vector>
using std::vector;

class Solution {
    private:
        static bool comp(vector<int> v, vector<int> w) {
            return v[0] == w[0] ? v[1] > w[1] : v[0] < w[0];
        }
    public:
        int removeCoveredIntervals(vector<vector<int>>& intervals) {
            std::sort(intervals.begin(), intervals.end());
            int ans = 0, end = -1;
            for (auto it = intervals.begin(); it != intervals.end(); ++it) {
                if (end < *it[1]) {
                    ++ans;
                    end = *it[1];
                }
            }
            return ans;
        }
};

int main() {
    // Compile successful
    return 0;
}
