// Day 21, Asteroid Collision
#include <algorithm>
#include <iostream>
#include <vector>
using std::vector;

class Solution {
    private:
        template <typename T> signum(T val) {
            return (T(0) < val) - (val < T(0));
        }
    public:
        vector<int> asteroidCollision(vector<int>& asteroids) {
            if (asteroids.size() <= 1) return asteroids;
            // Resolving collisions
            vector<int> leftover;
            for (int a : asteroids) {
                // Conditions where no collision occur
                if (a > 0 || leftover.empty() || leftover.back() < 0) {
                    leftover.push_back(a);
                    continue;
                }
                // Needs to resolve collision now
                while (a <= 0 &&  !leftover.empty() && leftover.back() >= 0) {
                    int b = leftover.back();
                    leftover.pop_back();
                    if (a + b > 0) {
                        leftover.push_back(b);
                        break;
                    } else if (a + b == 0) {
                        break;
                    } else if (leftover.empty() || leftover.top() < 0) {
                        leftover.push(a);
                    }
                }
            }
            // Constructing leftover
            return leftover;
        }
};

int main() {
    // Compile successful
    return 0;
}
