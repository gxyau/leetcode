// Day 22, Minimum Depth of Binary Tree
#include <iostream>

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
    private:
        int find_depth(TreeNode* root, int level) {
            if (!(root->left || root->right)) return level;
            if (!root->left) return find_depth(root->right, level+1);
            if (!root->right) return find_depth(root->left, level+1);
            return std::min(find_depth(root->left, level+1), find_depth(root->right, level+1));
        }
    public:
        int minDepth(TreeNode* root) {
            if (!root) return 0;
            return find_depth(root,1);
        }
};

int main() {
    // Compile successful
    return 0;
}
