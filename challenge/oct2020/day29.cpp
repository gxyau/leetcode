// Day 29, Maximize Distance to Closest Person
#include <vector>
using std::vector;

class Solution {
    public:
        int maxDistToClosest(vector<int>& seats) {
            int n = seats.size();
            vector<int> left(n), right(n);
            int count = 0;
            bool v = false;
            for(int i = 0; i < n; i++){
                if(seats[i] == 1){
                    count = 0;
                    v = true;
                }else{
                    count++;
                }
                if(!v){
                    count = 0;
                }
                left[i] = count;
            }
            v = false;
            for(int i = n- 1; i >= 0; i--){
                if(seats[i] == 1){
                    count = 0;
                    v = true;
                }else{
                    count++;
                }
                if(!v){
                    count = 0;
                }
                right[i] = count;
            }
            int ans = 0;
            for(int i = 0; i < n; i++){
                ans = std::max(ans, std::min(left[i], right[i]));
                if(left[i] == 0 || right[i] == 0){
                    ans = std::max(ans, std::max(left[i], right[i]));
                }
            }
            return ans;
        }
};

int main() {
    return 0;
}
