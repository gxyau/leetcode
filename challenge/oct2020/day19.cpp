// Day 19, Minimum Domino Rotations For Equal Row
#include <algorithm>
#include <iostream>
#include <vector>
using std::vector;

class Solution {
    private:
        int count(vector<int> &A, vector<int> &B, int num) {
            const int N = A.size();
            int countA = 0, countB = 0;
            for (int i = 0; i < N; ++i) {
                if (A[i] != num && B[i] != num) return -1;
                if (A[i] != num) ++countA;
                if (B[i] != num) ++countB;
            }
            return std::min(countA, countB);
        }
    public:
        int minDominoRotations(vector<int>& A, vector<int>& B) {
            int swapA = count(A,B,A[0]), swapB = count(A,B,B[0]);
            return (std::min(swapA, swapB) >= 0) ? std::min(swapA, swapB) : std::max(swapA, swapB);
        }
};

int main() {
    // Compile successful
    return 0;
}
