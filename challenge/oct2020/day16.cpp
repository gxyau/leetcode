// Day 16, Search a 2D Matrix
#include <iostream>
#include <vector>
using std::vector;

class Solution {
    public:
        bool searchMatrix(vector<vector<int>>& matrix, int target) {
            if (matrix.empty() || matrix[0].empty()) return false;
            const int m = matrix.size(), n = matrix[0].size();
            int left,right,mid,row,col;
            // Getting row coordinate
            left = 0, right = m*n;
            while (left <= right) {
                mid = left + (right - left)/2;
                if (mid >= m*n) return false;
                row = mid/n;
                col = mid%n;
                if (matrix[row][col] == target) {
                    return true;
                } else if (matrix[row][col] < target) {
                    left = mid + 1;
                } else {
                    right = mid - 1;
                }
            }
            return false;
        }
};

int main() {
    // Compile successful
    return 0;
}
