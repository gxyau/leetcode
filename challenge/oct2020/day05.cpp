// Day 5, Complement of Base 10 Integer
#include <iostream>

class Solution {
    public:
        int bitwiseComplement(int N) {
            if (N == 0) return 1;
            int com = 0, index = 0;
            while (N) {
                com |= !(N & 1) << index;
                ++index;
                N >>= 1;
            }
            return com;
        }
};

int main() {
    Solution sol;
    int N, com;
    // Little program
    std::cout << "Please enter a non-negative integer (< 10^9): ";
    std::cin  >> N;
    com = sol.bitwiseComplement(N);
    std::cout << "The complement is: " << com << std::endl;
    // Compile successful
    return 0;
}
