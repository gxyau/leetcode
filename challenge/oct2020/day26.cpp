// Day 26, Champagne Tower
#include <vector>;
using std::vector;

class Solution {
    public:
        double champagneTower(int poured, int query_row, int query_glass) {
            double flow[100] = {0};
            flow[0] = poured;
            for (int i = 0; i < query_row; ++i) {
                // Start from the end to keep track of volume poured
                for (int j = i; j >= 0; --j) {
                    double overflow = std::max(0.0, (flow[j] - 1.0)/2);
                    flow[j]    = overflow;
                    flow[j+1] += overflow;
                }
            }
            return std::min(1.0,flow[query_glass]);
        }
};

int main() {
    // Compile successful
    return 0;
}
