// Day 2, Combination Sum
#include <algorithm>
#include <iostream>
#include <vector>
using std::vector;

class Solution {
    private:
        vector<bool> achievable;
        void DP(vector<vector<int>> &results, vector<int> &current, vector<int> &candidates, int target, int index) {
            if (target == 0) {
                results.push_back(current);
                return; 
            }
            for (int i = index; i < candidates.size(); ++i) {
                int x = candidates[i];
                if (x > target) break;
                current.push_back(x);
                DP(results, current, candidates, target-x, i);
                current.pop_back();
            }
            return;
        }
    public:
        vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
            vector<vector<int>> results;
            vector<int> current;
            int index = 0;
            std::sort(candidates.begin(), candidates.end());
            DP(results, current, candidates, target, index);
            return results;
        }
};

int main() {
    // Compile successful
    return 0;
}
