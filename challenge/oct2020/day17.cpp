// Day 17, Find DNA Sequences
#include <iostream>
#include <sttring>
#include <unordered_set>
#include <vector>
using std::string;
using std::unordered_set;
using std::vector;

class Solution {
    public:
        char t[20] = {0, -1, 1, -1, -1, -1, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 3};
        // A = 0, C = 1, G = 2, T = 3, others = -1
        int getKey(string& s, int idx) {
            int key = 0;
            for (int i=0; i<10; ++i) key = (key << 2) | (t[s[idx+i] - 'A']);
            // represents the 10 length nucleic sequene into 20 bits
            return key;
        }

        vector<string> findRepeatedDnaSequences(string s) {
            unordered_map<int, int> m;
            vector<string> A;
            for (int i=0; i+10 <= s.size(); ++i) {
                int key = getKey(s, i);
                if (m.find(key) != m.end()) {
                    if (m[key] == 1) A.push_back(s.substr(i, 10));
                    ++m[key];
                } else m[key] = 1;
            }
            return A;
        }
};

int main() {
    // Compile successful
    return 0;
}
