// Day 10, Minimum Number of Arrows to Burst Balloons
#include <algorithm>
#include <iostream>
#include <vector>
using std::vector;

class Solution {
    private:
        static bool comp (vector<int> &v, vector<int> &w) {return v[1] < w[1];}
    public:
        int findMinArrowShots(vector<vector<int>>& points) {
            if (points.empty()) return 0;
            // Sorting points
            std::sort(points.begin(), points.end(), comp);
            // Greedy algorithm
            int total = 1, end = points[0][1];
            for (auto vec : points) {
                if (vec[0] > end) {
                    ++total;
                    end = vec[1];
                }
            }
            return total;
        }
};

int main() {
    // Compile successful
    return 0;
}
