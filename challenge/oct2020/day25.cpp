// Day 25, Stone Game IV
#include <cmath>
#include <vector>
using std::vector;

class Solution {
    public:
        bool winnerSquareGame(int n) {
            vector<bool> vec(n+1,{false});
            // Dynamic programming
            for(int i = 1; i <= n; ++i) {
                int k = (int) std::floor(std::sqrt(i)*1.0);
                while(k && !vec[i]){
                    vec[i] = !vec[i-k*k];
                    --k;
                }
            }
            return vec[n];
        }
};

int main() {
    // Compile successful
    return 0;
}
