// Day 31, Recover Binary Search Tree

// Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


using Node = TreeNode;

class Solution {
    public:
        void recoverTree(TreeNode* root) {
          Node* prev = nullptr;
          Node* first_not_sorted = nullptr;
          Node* min_node = nullptr;
          bool is_sorted = true;
          function<void(Node*)> visit_sorted;
          visit_sorted = [&](Node* node) {
            if (!node) return;
            visit_sorted(node->left);
            if (!prev) {
              prev = node;
            }
            else if (prev->val > node->val && is_sorted) {
              is_sorted = false;
              first_not_sorted = prev;
              min_node = node;
            }
            prev = node;
            if (!is_sorted) {
              if (min_node->val > node->val) {
                min_node = node;
              }
            }
            visit_sorted(node->right);
          };
          visit_sorted(root);
          std::swap(min_node->val, first_not_sorted->val);
        }
};

int main() {
    // Compile successful
    return 0;
}
