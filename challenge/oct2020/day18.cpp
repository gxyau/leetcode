// Day 18, Best Time to Buy and Sell Stock IV
#include <iostream>
#include <limits>
#include <vector>
using std::vector;

class Solution {
    public:
        int maxProfit(int k, vector<int>& prices) {
            if(prices.size() <= 1 || k == 0) return 0;
            const int N = prices.size();
            int profit;
            if(k >= N/2){
                profit = 0;
                for(int i = 1; i < N; ++i){
                    if(prices[i] > prices[i-1])
                        profit += prices[i] - prices[i-1];
                }
                return profit;
            } else {
                vector<vector<int>> DP(k+1,vector<int>(N,{0}));
                for(int i = 1; i < k+1; ++i){
                    profit  = std::numeric_limits<int>::min();
                    for(int j = 1; j < N; ++j){
                        profit = max(profit , DP[i-1][j-1] - prices[j-1]);
                        DP[i][j] = max(DP[i][j-1], prices[j] + profit);
                    }
                }
                return DP[k][N-1];
            }  
        }
};

int main() {
    // Compile successful
    return 0;
}

