// Day 20, Clone Graph
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>
using std::vector;

// Definition for a Node.
class Node {
    public:
        int val;
        vector<Node*> neighbors;

        Node() {
            val = 0;
            neighbors = vector<Node*>();
        }

        Node(int _val) {
            val = _val;
            neighbors = vector<Node*>();
        }

        Node(int _val, vector<Node*> _neighbors) {
            val = _val;
            neighbors = _neighbors;
        }
};

class Solution {
    private:
        // Unordered map to remember counterpart in clone
        std::unordered_map<Node*, Node*> map;
        Node* DFS(Node *node) {
            // If clone already exists
            if (map.count(node)) return map[node];
            Node* copy = new Node(node->val);
            map[node] = copy;
            for (auto v : node->neighbors) copy->neighbors.push_back(DFS(v));
            return copy;
        }
    public:
        Node* cloneGraph(Node* node) {
            // If it is empty graph, return empty graph
            if (!node) return nullptr;
            return DFS(node);
        }
};

int main() {
    // Compile successful
    return 0;
}
