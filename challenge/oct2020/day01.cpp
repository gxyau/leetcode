// Number of Recent Calls
// Implementing the RecentCounter class
#include <algorithm>
#include <iostream>
#include <vector>
using std::vector;

class RecentCounter {
    private:
        vector<int> requests;
    public:
        RecentCounter() {
            requests = vector<int>(0);
        }
        
        int ping(int t) {
            // We have to push the request into the vector regardless
            requests.push_back(t);
            // We find the lower bound because we need to include current request
            vector<int>::iterator it = std::lower_bound(requests.begin(), requests.end(),t-3000);
            return requests.end() - it;
        }
};
