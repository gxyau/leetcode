// Day 28, Summary Ranges
#include <string>
#include <vector>
using std::string;
using std::vector;

class Solution {
    public:
        vector<string> summaryRanges(vector<int>& nums) {
            vector<string> ranges = {};
            if (nums.empty()) return ranges;
            string current = std::to_string(nums[0]);
            int start = nums[0], tail = nums[0];
            for (auto it = nums.begin()+1; it != nums.end(); ++it) {
                if (*it == *(it-1)+1) {
                    tail = *it;
                    continue;
                } else {
                    tail = *(it-1);
                }
                if (start < tail) current += "->" + std::to_string(tail);
                ranges.push_back(current);
                start   = *it;
                current = std::to_string(start);
            }
            if (start < tail) current += "->" + std::to_string(tail);
            ranges.push_back(current);
            return ranges;
        }
};

int main() {
    // Compile successful
    return 0;
}
