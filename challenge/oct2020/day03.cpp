// Day 3, K-diff Pairs in an Array
#include <iostream>
#include <unordered_map>
#include <vector>
using std::vector;

class Solution {
    public:
        int findPairs(vector<int>& nums, int k) {
            std::unordered_map<int, int> map;
            int count = 0;
            for (int x : nums) ++map[x];
            for (std::pair<int, int> p : map) {
                if ( (k > 0 && map.count(p.first-k)) || (k == 0 && map[p.first] >= 2)) {
                    ++count;
                }
            }
            return count;
        }
};

int main() {
    Solution sol;
    vector<int> nums;
    int k, pairs;
    // Test cases
    // Test case 1
    nums = {3,1,4,1,5};
    k    = 2;
    pairs = sol.findPairs(nums, k);
    std::cout << "There are " << pairs << " pairs" << std::endl;
    // Compile successful
    return 0;
}
