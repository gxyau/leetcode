// Day 7, Rotate List
#include <iostream>

// Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
    public:
        ListNode* rotateRight(ListNode* head, int k) {
            if (k == 0 || head == nullptr) return head;
            ListNode *tmp = head, *tail = head;
            int len = 1;
            while (tail->next) {
                ++len;
                tail = tail->next;
            }
            if (k >= len) k = k%len;
            for (int i = 0; i < len-k-1; ++i) {
                tmp = tmp->next;
            }
            tail->next = head;
            head       = tmp->next;
            tmp->next  = nullptr;
            return head;
        }
};

int main() {
    Solution sol;
    ListNode *head, *rotated, *current;
    int k, index;
    // Test cases
    // Test case 1
    head = nullptr;
    k    = 0;
    std::cout << "Original list: ";
    while (head) {
        std::cout << head->val << " ";
        head = head->next;
    }
    std::cout << std::endl;
    rotated = sol.rotateRight(head, k);
    std::cout << "Rotated list: ";
    while (rotated) {
        std::cout << rotated->val << " ";
        rotated = rotated->next;
    }
    std::cout << std::endl;
    std::cout << std::endl;
    // Test case 2
    head    = new ListNode(1);
    current = head;
    k       = 0;
    std::cout << "Original list: ";
    while (current) {
        std::cout << current->val << " ";
        current = current->next;
    }
    std::cout << std::endl;
    rotated = sol.rotateRight(head, k);
    std::cout << "Rotated list: ";
    while (rotated) {
        std::cout << rotated->val << " ";
        rotated = rotated->next;
    }
    std::cout << std::endl;
    std::cout << std::endl;
    // Test case 3
    head    = new ListNode(1);
    current = head;
    k       = 1;
    std::cout << "Original list: ";
    while (current) {
        std::cout << current->val << " ";
        current = current->next;
    }
    std::cout << std::endl;
    rotated = sol.rotateRight(head, k);
    std::cout << "Rotated list: ";
    while (rotated) {
        std::cout << rotated->val << " ";
        rotated = rotated->next;
    }
    std::cout << std::endl;
    std::cout << std::endl;
    // Test case 4
    head    = new ListNode(1);
    current = head;
    k       = 2;
    std::cout << "Original list: ";
    while (current) {
        std::cout << current->val << " ";
        current = current->next;
    }
    std::cout << std::endl;
    rotated = sol.rotateRight(head, k);
    std::cout << "Rotated list: ";
    while (rotated) {
        std::cout << rotated->val << " ";
        rotated = rotated->next;
    }
    std::cout << std::endl;
    std::cout << std::endl;
    // Test case 5
    head    = new ListNode(1);
    current = head;
    k       = 2;
    index   = 5;
    while (--index) {
        current->next = new ListNode(6-index);
        current = current->next;
    }
    std::cout << "Original list: ";
    current = head;
    while (current) {
        std::cout << current->val << " ";
        current = current->next;
    }
    std::cout << std::endl;
    rotated = sol.rotateRight(head, k);
    std::cout << "Rotated list: ";
    while (rotated) {
        std::cout << rotated->val << " ";
        rotated = rotated->next;
    }
    std::cout << std::endl;
    std::cout << std::endl;
    // Test case 6
    head    = new ListNode(0);
    current = head;
    k       = 4;
    index   = 3;
    while (--index) {
        current->next = new ListNode(3-index);
        current = current->next;
    }
    std::cout << "Original list: ";
    current = head;
    while (current) {
        std::cout << current->val << " ";
        current = current->next;
    }
    std::cout << std::endl;
    rotated = sol.rotateRight(head, k);
    std::cout << "Rotated list: ";
    while (rotated) {
        std::cout << rotated->val << " ";
        rotated = rotated->next;
    }
    std::cout << std::endl;
    std::cout << std::endl;
    // Compile successful
    return 0;
}
