// Day 12, Buddy Strings
#include <algorithm>
#include <bitset>
#include <iostream>
#include <string>
using std::string;

class Solution {
    public:
        bool buddyStrings(string A, string B) {
            if (A.size() != B.size() || A.size() < 2) return false;
            string s,t;
            std::bitset<128> bits{0};
            bool dup = false;
            for (int i = 0; i < A.size() && s.size() <= 2; ++i) {
                if (A[i] != B[i]) {
		        	s += A[i], t += B[i];
                } else {
		        	dup |= bits.test(A[i]);
        			bits.set(A[i]);
		        }
            }
            if (s.size() == 2) {
                std::swap(s[0], s[1]);
                return s == t;
            }
            return s.empty() && dup;
        }
};

int main() {
    // Compile successful
    return 0;
}
