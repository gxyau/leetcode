// Day 23, 132 Pattern
#include <limits>
#include <stack>
#include <vector>
using std::stack;
using std::vector;

class Solution {
    public:
        bool find132pattern(vector<int>& nums) {
            // Needs at least 3 numbers in nums
            if (nums.size() < 3) return false;
            // Constants and variables
            const int n = nums.size();
            // Track minimum value from 0 to i-1
            vector<int> min_values(n,{std::numeric_limits<int>::max()});
            for (int i = 1; i < n; ++i) {
                min_values[i] = std::min(min_values[i-1], nums[i-1]);
            }
            // Using stack to keep track of last number
            stack<int> st;
            for (int i = n-1; i >= 0; --i) {
                while (!st.empty() && st.top() <= min_values[i]) st.pop();
                if (!st.empty() && st.top() < nums[i]) return true;
                st.push(nums[i]);
            }
            return false;
        }
};

int main() {
    // Compile successful
    return 0;
}
