// Day 27, Linked Cycle II

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

class Solution {
    public:
        ListNode *detectCycle(ListNode *head) {
            ListNode *fast = head, *slow = head;
            while(fast && fast->next){
                fast = fast->next->next;
                slow = slow->next;
                if(fast == slow) break;
            }
            if (!fast || !fast->next) return nullptr;
            slow = head; // Return pointer to head
            while(fast != slow){
                fast = fast->next;
                slow = slow->next;
            }
            return fast;
        }
};

int main() {
    // compile successful
    return 0;
}
