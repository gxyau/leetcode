#include <stdio.h>

void echo(char* s) {
    printf(s);
    printf("\n");
    
    return ;
}

int main(int argc, char** argv) {
    printf("This is a C template file to compile leetcode problems.\n\n");

    printf("There are %d arguments\n", argc);
    
    printf("The arguments are as follows:\n");
    for (int i = 0; i < argc; ++i){
        echo(argv[i]);
    }
    
    return 0;
}
