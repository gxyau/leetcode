void swap(int* x, int* y){
    int tmp = *x;
    *x = *y;
    *y = tmp;
}
    

int removeDuplicates(int* nums, int numsSize) {
    //if (numsSize == 1) {return 1;}
    int k = 1, index = 1, current = nums[0];
    
    for (int i = 0; i < numsSize; ++i) {
        if (*(nums+i) != current) {
            current = *(nums+i);
            swap((nums+index), (nums+i));
            ++index;
            ++k;
        }
    }
    
    return k;
}