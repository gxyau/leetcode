// Problem 5, Longest Palindromic Substring
#include <string>
#include <vector>
using std::string;
using std::vector;

class Solution {
    public:
        string longestPalindrome(string s) {
            if (s.size() < 2) return s;
            const size_t n = s.size();
            // Dynamic programming
            int max_len = 1, start = 0;
            for (int i = 0; i < n; ++i) {
                int j = i, k = i;
                while (k+1 < n && s[k] == s[k+1]) ++k;
                while (j-1 >= 0 && k+1 < n && s[j-1] == s[k+1]) {
                    ++k;
                    --j;
                }
                if(max_len < k-j+1) {
                    max_len = k-j+1;
                    start = j;
                }
            }
            return s.substr(start, max_len);
        }
};

int main() {
    // Exit successful
    return 0;
}
