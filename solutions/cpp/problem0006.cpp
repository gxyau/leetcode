// Problem 6, ZigZag Conversion
#include <string>
using std::string;

class Solution {
    public:
        string convert(string s, int num_rows) {
            if (num_rows == 1) return s;
            int cycle = 2 * num_rows - 2;
            string ans = "";
            for (int i = 0; i < num_rows; ++i) {
                for (int j = 0; j+i < s.size(); j += cycle) {
                    ans += s[j+i];
                    if (i != 0 && i != num_rows - 1 && j + cycle - i < s.size())
                        ans += s[j + cycle - i];
                }
            }
            return ans;
        }
};

int main() {
    // Exit successful
    return 0;
}
