// Problem 1081, Smallest Subsequence of Distinct Characters
#include <iostream>
#include <string>
using std::string;

class Solution {
    public:
        string smallestSubsequence(string s) {
            const int N = s.size();
            int unique = 0;
            string res = "";
            if (N <= 1) return s;
            // Counting frequency of letters
            int freq[26] = {0};
            for (char x : s) {
                if (freq[x-'a'] == 0) ++unique;
                ++freq[x-'a'];
            }
            // If they are all unique
            if (N == unique) return s;
            // Building final string
            for (char x : s) {                while ( (res.size() > 0) && (res.back() > x) && (freq[res.back()-'a'] > 0) && (res.find(x) == string::npos)) {
                    res.pop_back();
                }
                if (res.find(x) == string::npos) {
                    res += x;
                }
                --freq[x-'a'];
            }
            return res;
        }
};

int main() {
    Solution sol;
    string s, t;
    // Mini program
    std::cout << "Please enter a string: ";
    std::cin  >> s;
    t = sol.removeDuplicateLetters(s);
    std::cout << "Smallest unique string: " << t << std::endl;
    // Compile successful
    return 0;
}
