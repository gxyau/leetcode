// Problem 7, Reverse Integer
#include <iostream>
#include <limits>

class Solution {
    public:
        int reverse(int x) {
            int ans = 0;
            while (x) {
                ans *= 10;
                ans += (x%10);
                x   /= 10;
            }
            return (ans > std::numeric_limits<int>::max() || ans < std::numeric_limits<int>::min()) ? 0 : ans ;
        }
};

int main() {
    // Variables
    Solution sol;
    int x, ans;
    // Small program
    std::cout << "Please enter an integer: ";
    std::cin  >> x;
    ans = sol.reverse(x);
    std::cout << "Reverse: " << ans << std::endl;
    // Exit successful
    return 0;
}
