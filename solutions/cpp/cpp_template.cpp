// Leetcode template for C++
#include <iostream>
#include <string>
#include <vector>
using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;

class Solution {
    private:
    public:
        string echo(string s) {
            cout << "The string is: " << s << endl;
            return s;
        }

        vector<int> count(int n) {
            vector<int> vec;
            for (int i = 0; i < n; ++i){
                vec.push_back(i);
            }
            return vec;
        }
};

int main(int argc, char** argv) {
    Solution sol;

    cout << "This is a template for C++ solutions in leetcode." << endl << endl;
    cout << "Number of arguments: " << argc << endl;
    cout << "Vector: ";
    for (auto x : sol.count(argc)) {
        cout << x;
    }
    cout << endl << endl;

    cout << "The arguments are as follows:" << endl;
    for (int i = 0; i < argc; ++i) {
        cout << sol.echo(argv[i]) << endl;
    }
    cout << endl;

    return 0;
}
